export class User {
  uid: string;
  firstname: string;
  lastname: string;
  password: string;
  email: string;
}
