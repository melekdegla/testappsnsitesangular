import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NavbarComponent} from './navbar/navbar.component';
import {SignupComponent} from './signup/signup.component';
import {LoginComponent} from './login/login.component';
import {ListComponent} from './list/list.component';
import {AuthGuard} from './auth.guard';


const routes: Routes = [{
  path: '',
  component: NavbarComponent,
  children: [{
    path: '',
    component: SignupComponent
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: 'list',
    component: ListComponent,
    canActivate: [AuthGuard]
  }]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
