import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import {User} from '../models/user';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  /*
  * this service using firebase lib
  * if we are using normal rest api we should use http client of rxjs
  * */
  private users: AngularFireList<User[]>;
  constructor(private db: AngularFireDatabase, private af: AngularFireAuth) { }

  addUser(user) {
    this.users = this.db.list('users');
    this.af.auth.createUserWithEmailAndPassword(user.email, user.password).then(r => {
      this.users.set(r.user.uid, user);
    });
  }
  list() {
    return this.db.list('users');
  }
  signin(user) {
   return this.af.auth.signInWithEmailAndPassword(user.email, user.password);
  }
  /*
  * this auth with firebase lib
  * if we are using rest api backend we should use jwt with interceptor of authorization for each request*/
  isAuth() {
    console.log(this.af.authState);
    return this.af.authState ;
  }
  logout() {
    this.af.auth.signOut();
  }

  delete(uid: any) {
    this.db.object('users/' + uid).remove();
  }
}
