import { Component, OnInit } from '@angular/core';
import {User} from '../models/user';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  user: User;
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.user = new User();
    this.dataService.isAuth().subscribe(r => console.log(r));
  }
  add() {
    this.dataService.addUser(this.user);
  }

}
