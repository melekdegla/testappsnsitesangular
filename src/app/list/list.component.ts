import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  list: any = [];
  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.list().snapshotChanges().subscribe(r => {
      this.list = [];
      r.forEach(u => {
        const user = u.payload.toJSON();
        user.uid = u.key;
        this.list.push(user);
        console.log(this.list);
      });

    });
  }
  delete(uid) {
    this.data.delete(uid);
  }

}
