import { Component, OnInit } from '@angular/core';
import {User} from '../models/user';
import {DataService} from '../services/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User;

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.user = new User();
    this.dataService.isAuth().subscribe(r => {
      if (r !== null) {
        this.router.navigateByUrl('/list');

      }
    });
  }
   login() {
    this.dataService.signin(this.user).then(r => console.log('done'));
   }

}
